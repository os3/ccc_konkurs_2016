# CCC Konkurs 2016

## Libraries used
- jQuery
- fabric.js
- fabric.js custom controls

## Setup
You will need to set up the following virtual host for this project to build/serve. Don't forget to change the PATH.
```
<VirtualHost *:80>
    DocumentRoot "PATH/ccc_konkurs_2016/dist"
    ServerName ccc-konkurs.dev
    <Directory PATH/ccc_konkurs_2016/dist>
        Options Indexes FollowSymLinks
        AllowOverride all
        Order allow,deny
        allow from all
        Require all granted
    </Directory>
</VirtualHost>
```
also, add the following line to etc/hosts:
```
127.0.0.1   ccc-konkurs.dev
```
Finally, go to the project root directory and install all node packages listed as dependencies in package.json. Try the following command:
```
(sudo) npm install
```
If that does not work, you will need to install the packages separately.
```
(sudo) npm install gulp gulp-load-plugins browser-sync gulp-uglify gulp-concat gulp-sass gulp-uglifycss gulp-concat-css gulp-postcss gulp-sourcemaps autoprefixer gulp-rename es6-promise del bower
```
After installing all the node packages, also install all bower deps.
```
bower install
```

## Build / Server
### Build
```
gulp build
```
This is the first command you want to run. It will build vendors.js, copy assets (images) to /dist and finally create html and minified js and css in /dist.  

### Serve / Watch
```
gulp
```
This will start a browsersync session hosted from dist. It will watch for changes in CSS, JS and HTML and will replace these files and reload the browser. It is important to remember that this command DOES NOT build vendors.js or copy assets. If you lose these files in /dist, rebuild them with gulp build. It makes no sense to do this processor-intensive operation every time a browser refresh is needed.
