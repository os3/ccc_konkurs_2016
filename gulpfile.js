// VARS ////////////////////////////////////////////////////////////////////////////////////////////
var gulp            = require('gulp');
var $               = require('gulp-load-plugins')();
var browserSync     = require('browser-sync').create();
var reload          = browserSync.reload;
var uglify          = require('gulp-uglify');
var concat          = require('gulp-concat');
var sass            = require('gulp-sass');
var uglifycss       = require('gulp-uglifycss');
var concatCss       = require('gulp-concat-css');
var postcss         = require('gulp-postcss');
var sourcemaps      = require('gulp-sourcemaps');
var autoprefixer    = require('autoprefixer');
var rename          = require('gulp-rename');
var Promise         = require('es6-promise').polyfill();
var del             = require('del');

// BUILD ///////////////////////////////////////////////////////////////////////////////////////////
// CONCATENATE //
gulp.task('concat_js', function () {
    return gulp.src([
        './bower_components/jquery/dist/jquery.js',
        './bower_components/fabric/dist/fabric.js',
        './bower_components/fabric-customise-controls/dist/customiseControls.js'
    ])
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest('./src/temp'));
});

// MINIFY //
gulp.task('minify_js',['concat_js'], function () {
    gulp.src('./src/js/scripts.js')
    .pipe(uglify())
    .pipe(gulp.dest('./dist/assets/js'));
    gulp.src('./src/temp/vendors.js')
    .pipe(uglify())
    .pipe(gulp.dest('./dist/assets/js'));
});

gulp.task('minify_css',['autoprefixer'], function () {
    gulp.src('./src/temp/style-autoprefix.css')
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": false
    }))
    .pipe(rename("style.css"))
    .pipe(gulp.dest('./dist/assets/css'));
});

// SASS //
gulp.task('sass', function () {
    gulp.src('./src/scss/**/*.scss')
        .pipe($.sass())
        .pipe(gulp.dest('./src/temp'));
});

gulp.task('autoprefixer',['sass'], function () {
    return gulp.src('./src/temp/style.css')
        .pipe(postcss([ autoprefixer({ browsers: ['> 1%'], cascade:false }) ]))
        .pipe(rename("style-autoprefix.css"))
        .pipe(gulp.dest('./src/temp'));
});

// COPY //
gulp.task('html', function () {
    gulp.src('./src/html/index.html')
    .pipe(gulp.dest('./dist'));
});

gulp.task('assets', function () {
    gulp.src('./src/img/**/*')
    .pipe(gulp.dest('./dist/assets/img'));
});

// SERVE ///////////////////////////////////////////////////////////////////////////////////////////
// Helpers:
gulp.task('minify_js_serve', function () { // skip building vendors
    gulp.src('./src/js/scripts.js')
    .pipe(uglify())
    .pipe(gulp.dest('./dist/assets/js'));
});

gulp.task('serve', function() {

    browserSync.init({
        proxy: "ccc-konkurs.dev",
        port: 4444,
        open: true,
        notify: false
    });

    gulp.watch('./src/js/*', ['minify_js_serve']);
    gulp.watch('./src/html/*', ['html']);
    gulp.watch('./src/scss/**/*', ['minify_css']);
    gulp.watch('./dist/**/*').on('change', reload);
});

// ALIASES //
gulp.task('default', ['serve']);
gulp.task('build', ['minify_css','minify_js','html','assets']);
