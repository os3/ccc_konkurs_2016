// Declare canvas elements
var image1 = {
	canvas: null,
	init: function() {
		image1.canvas = new fabric.Canvas('c');
	}
};

var image2 = {
	canvas: null,
	init: function() {
		image2.canvas = new fabric.Canvas('d');
	}
};
window.onload = function() { image1.init(); image2.init(); };

// Customize controls for canvas items
fabric.Object.prototype.customiseCornerIcons({
	settings: {
		borderColor: 'teal',
		cornerSize: 24,
		cornerShape: 'circle',
		cornerBackgroundColor: 'teal',
		cornerPadding: 0
	},
	mtr: {
		icon: 'assets/img/rotate.svg'
	},
	tl: {
		icon: 'assets/img/arrow-tl-br.svg'
	},
	mt: {
		icon: 'assets/img/arrow-mt-mb.svg'
	},
	tr: {
		icon: 'assets/img/arrow-tr-bl.svg'
	},
	mr: {
		icon: 'assets/img/arrow-mr-ml.svg'
	},
	ml: {
		icon: 'assets/img/arrow-mr-ml.svg'
	},
	bl: {
		icon: 'assets/img/arrow-tr-bl.svg'
	},
	mb: {
		icon: 'assets/img/arrow-mt-mb.svg'
	},
	br: {
		icon: 'assets/img/arrow-tl-br.svg'
	}
});

// Resize and center image to fit into canvas
function getDimensions(ih, iw, ch, cw){
	var r = {};
	if(ih < iw) {
		r.height = ((ih * cw)/iw)-24;
		r.width = cw-24;
		r.top = ((ch - r.height)/2);
		r.left = 12;
	} else if (ih > iw) {
		r.width = ((iw * ch)/ih)-24;
		r.height = ch-24;
		r.left = ((cw - r.width)/2);
		r.top = 12;
	} else {
		r.width = cw-24;
		r.height = ch-24;
		r.left = 12;
		r.top = 12;
	}
	return r;
}

// Change image in canvas
function changeImage(can, file) {
	var canContainer;
	if(can == image1.canvas){
		canContainer = $('#canvasContainer1');
	} else if(can == image2.canvas){
		canContainer = $('#canvasContainer2');
	}
	canContainer.animate({opacity:0}, 300, function(){ // fadeOut canvas to replace image off-screen
		if(canContainer.hasClass('set')){
			can._objects[0].remove(); // remove previous image from DOM
		} else {
			canContainer.css('background-color', 'white');
			canContainer.addClass('set');
		}
		var reader = new FileReader();
		reader.onload = function (f) {
			var data = f.target.result;
			fabric.Image.fromURL(data, function (img) {
				var dim = getDimensions(img.height, img.width, can.height, can.width);
				var oImg = img.set({
					left: dim.left,
					top: dim.top,
					width: dim.width,
					height: dim.height,
					borderScaleFactor: 2,
					rotatingPointOffset: 32
				}).scale(1);
				can.add(oImg).renderAll();
				var a = can.setActiveObject(oImg);
				var dataURL = can.toDataURL({format: 'png', quality: 1});
			});
		};
		reader.readAsDataURL(file);
		canContainer.animate({opacity:1}, 350);
	});
}

// Local file upload into canvas by pressing "Browse" button
$('.fileChange').on('change', function (e) {
	var can;
	if($(this).is('#file1')){
		can = image1.canvas;
	} else if($(this).is('#file2')){
		can = image2.canvas;
	}
	var file = e.target.files[0];
	changeImage(can, file);
});

// Local file upload into canvas by drop from desktop
$(document).ready(function () {
	var drop = $(".drop");

	var ignoreDrag = function(e) {
		var event = typeof e.originalEvent != 'undefined' ? e.originalEvent : e;
		if (event.stopPropagation) {
			event.stopPropagation();
		}
		if (event.preventDefault) {
			event.preventDefault();
		}
	};

	drop.bind('dragover', ignoreDrag).bind('dragenter', ignoreDrag).bind('drop', function(e){
		var can;
		if($(this).is('#canvasContainer1')){
			can = image1.canvas;
		} else if($(this).is('#canvasContainer2')){
			can = image2.canvas;
		}
		e = (e&&e.originalEvent?e.originalEvent:window.event) || e;
		ignoreDrag(e);
		var files = (e.files||e.dataTransfer.files);
		var file = files[0];
		changeImage(can, file);
	});
});

$("#btn-Preview-Image").on('click', function () {
	var target = new fabric.Canvas('temp');

	// deselect all objects to create clean image
	image1.canvas.deactivateAllWithDispatch().renderAll();
	image2.canvas.deactivateAllWithDispatch().renderAll();

	// rasterize canvases
	var imgData1 = image1.canvas.toDataURL("image/png");
	var imgData2 = image2.canvas.toDataURL("image/png");

	// clear preview canvas
	if(target._objects.length !== 0){
		target._objects[1].remove();
		target._objects[0].remove();
	}

	// add rasters to preview canvas
	var img1 = new Image();
	img1.onload = function() {
		var fImg = new fabric.Image(this, {
			left: 0,
			top: 0,
			width:400,
			height:400,
			selectable: false
		});
		target.add(fImg);
	};
	img1.src = imgData1;

	var img2 = new Image();
	img2.onload = function() {
		var fImg = new fabric.Image(this, {
			left: 400,
			top: 0,
			width:400,
			height:400,
			selectable: false
		});
		target.add(fImg);
	};
	img2.src = imgData2;

});

$("#btn-Convert-Html2Image").on('click', function () {
	var getCanvas = document.getElementById('temp');
	var imgageData = getCanvas.toDataURL("image/jpeg", 1.0);
	var newData = imgageData.replace(/^data:image\/jpeg/, "data:application/octet-stream");
	$("#btn-Convert-Html2Image").attr("download", "your_pic_name.jpg").attr("href", newData);
});
